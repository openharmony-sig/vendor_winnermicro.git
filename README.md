# 使用Neptune快速上手OpenHarmony

## 预编译固件

| 镜像     | 下载地址                                                     |
| -------- | ------------------------------------------------------------ |
| 完整镜像 | [w800.fls](https://gitee.com/hihope_iot/images/blob/master/Neptune/w800.fls) |

## 烧录方式

### 工具下载

```
https://gitee.com/hihope_iot/docs/tree/master/Neptune/resource/SecureCRSecureFXPortable.rar
```

### SecureCRT使用步骤

1. 下载工具后解压缩

![IMG_256](https://gitee.com/hihope-neptune/figure/raw/master/Neptune/clip_image002.gif)

2. 打开上图的.exe应用程序

![IMG_256](https://gitee.com/hihope-neptune/figure/raw/master/Neptune/clip_image004.gif)


3. 对串口进行配置

![IMG_256](https://gitee.com/hihope-neptune/figure/raw/master/Neptune/clip_image006.gif)


4. 选择当前使用的COM口

![IMG_256](https://gitee.com/hihope-neptune/figure/raw/master/Neptune/clip_image008.gif)


5. 连接串口与断开串口

![IMG_256](https://gitee.com/hihope-neptune/figure/raw/master/Neptune/clip_image010.gif)


6. 进入烧录模式需要先按住“ESC”，然后再按一下板子的复位键，最后松开“ESC”

![IMG_256](https://gitee.com/hihope-neptune/figure/raw/master/Neptune/clip_image012.gif)


7. 选择烧录文件进行烧录

![IMG_256](https://gitee.com/hihope-neptune/figure/raw/master/Neptune/clip_image014.gif)

​	正常情况下，烧录w800.img就可以了

![IMG_256](https://gitee.com/hihope-neptune/figure/raw/master/Neptune/clip_image016.gif)

​	烧录过程中会有一个烧录过程的显示，过程中不要随便点击该页面以免过程中断，烧录完成后，再次按下复位建即可。

![IMG_256](https://gitee.com/hihope-neptune/figure/raw/master/Neptune/clip_image018.jpg)

## 编译方法

编译方法简单，但编译环境搭建相对复杂。
[参考](https://gitee.com/hihope_iot/docs/blob/master/Neptune/%E8%BD%AF%E4%BB%B6%E4%BD%BF%E7%94%A8%E6%89%8B%E5%86%8C.md#neptune%E8%BD%AF%E4%BB%B6%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97)

