# Copyright (C) 2021 HiHope Open Source Organization .
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#include "oem_auth_config.h"

//no server info,use default server info
#define USE_DEFAULT_SERVER_INF  1

char kitInfo[] = "1.0.0.301";

int OEMReadAuthServerInfo(char* buff, unsigned int len)
{
#ifdef USE_DEFAULT_SERVER_INF
	(void)buff;
	(void)len;
    return -1;
#else
	//copy server info to buff.
#endif
}

char* OEMLoadKitInfos(void)
{
    return kitInfo;
}
